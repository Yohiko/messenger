from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.views import View
from django.contrib.auth.models import User

####################################################################

from rest_framework import status
from rest_framework import permissions
from rest_framework import authentication
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly 
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.authentication import BasicAuthentication
from .serializers import *

###################################################################

class SendMessageView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = MessageSerializer
    authentication_classes = (BasicAuthentication,)

    def post(self, request):
        recipient = request.data.get('recipient')
        user = request.user.pk
        temporary = request.data.get('temporary')

        if user == recipient:
            return Response(status=status.HTTP_409_CONFLICT,data={'success': False,'message':"You're can't provide messages to yourself!"})

        if (
            request.FILES
            and request.FILES.get("attachment")
            or request.data.get("text")
        ):
                   
            serializer = self.serializer_classes(
                data=request.data, context={"request": request}
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'message has been created succsesfully!'})

        return Response(status=status.HTTP_401_UNAUTHORIZED, data={"success": False, "result": 'wrong fields! [you can use: text, recipient, attachment]'})

################################################################
class LoginAPIView(APIView):
    def post(self, request):
        username = request.POST.get("username")
        password = request.POST.get("password")

        if not username or not password:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data={"success": False, "result": 'username or password was not provided'})

        user = authenticate(request=request,username=username,password=password)

        if user:
            login(request, user)
            return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'you have been authorized succesfully'})

        return Response(status=status.HTTP_401_UNAUTHORIZED, data={"success": False, "result": 'wrong credentials! [you can use: login, password]'})

class UserSessionsAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = UserSessionListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request, pk):
        user = User.objects.filter(pk=pk).first()
        sessions = Session.objects.filter(members__in=[user])

        serializer = self.serializer_classes(sessions, many=True)

        return Response(status=status.HTTP_200_OK, data={'succes': True, 'result': serializer.data})


class UserMessagesAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserMessageListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request, pk):
        user = User.objects.get(pk=pk)
        message = Message.objects.filter(receiver=user)

        serializer = self.serializer_class(message, many=True)

        return Response(status=status.HTTP_200_OK, data={'succes': True, 'result': serializer.data})

class UserAttachmentAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserAttachmentListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request, pk):
        session = Session.objects.get(id_session=pk)
        messages = Message.objects.filter(session=session)
        attachment = Attachment.objects.filter(attachment__in=messages)
    
        serializer = self.serializer_class(attachment, many=True)

        return Response(status=status.HTTP_200_OK, data={'success': True, 'result': serializer.data})

class MessageReactionAPIView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = MessageRectionSerializer
    authentication_classes = (BasicAuthentication, )

    queryset = Message.objects.all()
    model = Message

    def patch(self, request, pk):
        serializer_object = self.get_object()
        serializer = MessageRectionSerializer(serializer_object, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK, data={'success': True,"result": "reaction updated successfully"})
        return Response(status=status.HTTP_409_CONFLICT, data={'success': False,"result": "update failed!"})


class AuthorizeUserSessionAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = UserMessageListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request):
        user_id = request.user.id 
        message = Message.objects.filter(receiver__in=[user_id])
        print(user_id, message)

        serializer = self.serializer_classes(message, many=True)

        return Response(status=status.HTTP_200_OK, data={'succes': True, 'result': serializer.data})

class SessionImagesAPIVIew(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserAttachmentListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request, pk):
        session = Session.objects.get(id_session=pk)
        messages = Message.objects.filter(session=session)
        files = ['jpeg','png','jpg',]
        attachments = Attachment.objects.filter(attachment__in=messages)
        photos = []

        for attachment in attachments:
            if attachment.file.name.split('.')[1] in files:
                photos.append(attachment)

        serializer = self.serializer_class(photos, many=True)

        return Response(status=status.HTTP_200_OK, data={'success': True, 'result': serializer.data})



###################################################################

class SessionDestroyAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BasicAuthentication, )

    queryset = Session.objects.all()
    model = Session

    def delete(self, request, pk):
        user_id = request.user.id 
        session = Session.objects.filter(members__in=[user_id], id_session=pk).first()
        delete = request.data.get('delete_from')

        print(session, user_id, delete)
        if int(delete) == int(1):
            print('delete passed')
            user = User.objects.filter(pk=request.user.pk)
            session: Session = Session.objects.filter(id_session=pk).first()
            if session:
                print('session passed')
                session.members.remove(user[0].id)
                return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'Session has been deleted succesfully! (only you)'})
            
            return Response(status=status.HTTP_409_CONFLICT, data={"success": False, "result": 'You are not in session!'})

        if int(delete) == int(2):
            user = User.objects.filter(pk=request.user.pk)
            session: Session = Session.objects.filter(id_session=pk).first()
            if session:
                session.delete()
                return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'Session has been deleted succesfully! (both)'})

            return Response(status=status.HTTP_409_CONFLICT, data={"success": False, "result": 'You are not in session!'})
        
        return Response(status=status.HTTP_409_CONFLICT, data={"success": False, "result": 'Something wrong, try again'})

class MessageDestroyAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BasicAuthentication, )

    queryset = Message.objects.all()
    model = Message

    def delete(self, request, pk):
        user_id = request.user.id 
        message = Message.objects.filter(pk=pk, receiver_id=user_id).first()
        print(message, user_id)
        if message:
            message.delete()
            return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'Message has been deleted succesfully!'})

        return Response(status=status.HTTP_409_CONFLICT, data={"success": False, "result": 'Such message does not exist!'})

class TemporaryMessageAPIVIew(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (BasicAuthentication, )

    queryset = Message.objects.all()
    model = Message

    def delete(self, request):
        user_id = request.user.id 
        message = Message.objects.filter(receiver_id=user_id).first()
        temporary = Message.objects.filter(temporary='1')

        print(temporary)
        
        if message:
            temporary.delete()
            return Response(status=status.HTTP_200_OK, data={"success": True, "result": 'Temporary messages have been deleted succesfully!'})
            
        return Response(status=status.HTTP_409_CONFLICT, data={"success": False, "result": 'Such message does not exist!'})