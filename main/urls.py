from . import views
from django.urls import path
from .views import *

urlpatterns = [
    path('api/message/send/', SendMessageView.as_view(), name="send_message"),

    ################################################################################3

    path('api/users/sessions/<int:pk>/', UserSessionsAPIView.as_view(), name='api-users-sessions'),
    path('api/users/message/<int:pk>/', UserMessagesAPIView.as_view(), name='api-users-message'),
    path('api/current-user/message/', AuthorizeUserSessionAPIView.as_view(), name='api-user-message'),
    path('api/session/images/<uuid:pk>/', SessionImagesAPIVIew.as_view(), name='api-session-images'),

    path('api/users/attachments/<uuid:pk>/', UserAttachmentAPIView.as_view(), name='api-users-attachments'), 
    path('api/users/messages/update/<int:pk>/', MessageReactionAPIView.as_view(), name='api-message-reactions'), 

    path('api/session/delete/<uuid:pk>/', SessionDestroyAPIView.as_view(), name='api-session-delete'),
    path('api/message/delete/<int:pk>/', MessageDestroyAPIView.as_view(), name='api-message-delete'),
    path('api/temporary-message/delete/', TemporaryMessageAPIVIew.as_view(), name='api-message-delete'),
]