from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields import related
from django.db.models.fields.files import FileField
from django.dispatch.dispatcher import receiver 
from django.utils import timezone
from .fields import ContentTypeRestrictedFileField
import uuid

REACTIONS = (
    ('1', 1), # <3
    ('2', 2), # -_-
    ('3', 3), # :(
    ('4', 4),  # :)
)

TEMPRORARY = (
    ('0', 0),
    ('1', 1),
)

DELETE_FROM = (
    ('1', 1),
    ('2', 2),
)

class Session(models.Model):
    id_session = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, blank=True)
    session_creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='session_creator', blank=True)
    members = models.ManyToManyField(User, related_name='members')
    delete_from = models.CharField(max_length=1, choices=DELETE_FROM, blank=True)
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self) -> str:
        return str(self.id_session)

class Message(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE, related_name='session_messages') 
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user1')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user2')
    text = models.TextField()
    reaction = models.CharField(max_length=1, choices=REACTIONS, blank=True)
    date_created = models.DateTimeField(default=timezone.now,editable=False)
    temporary = models.CharField(max_length=1, choices=TEMPRORARY)

    def __str__(self) -> str:
        return '%s %s' % (self.receiver, self.recipient)

class Attachment(models.Model):
    attachment = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='attachment')
    file = ContentTypeRestrictedFileField(
        upload_to = 'media/',
        verbose_name = ('pinned files'),
        content_types = [
            'image/jpeg',
            'image/png',
            'image/jpg',
            'video/mp4',
            'video/ogg',
            'text/plain',
            'audio/mp4',
        ]
    )
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self) -> str:
        return str(self.attachment)