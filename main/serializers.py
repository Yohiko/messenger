from rest_framework.serializers import ModelSerializer, UUIDField
from django.contrib.auth.models import User
from .models import *
import uuid
from django.db.models import Q

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')

    def create(self, validated_data):
        user = User(**validated_data)
        user.save()

        return user

class SessionListSerializer(ModelSerializer):
    class Meta:
        model = Session
        fields = ('id_session','session_creator','session_members','date_created')
    
    def create(self, validated_data):
        session = User(**validated_data)
        session.save()

        return session

    class Meta:
        model = Message
        fields = ('session','receiver','recipient','text','reaction','date_created')

    def create(self, validated_data):
        message = User(**validated_data)
        message.save()

        return message

class UserAttachmentListSerializer(ModelSerializer):
    class Meta:
        model = Attachment
        fields = "__all__"


class MessageSerializer(ModelSerializer):
    def create(self, validated_data):
        user = self.context.get("request").user
        validated_data.update({"receiver_id": user.id})

        sender_sessions = Session.objects.filter(members__in=[user])
        msg_session = sender_sessions.filter(members__in=[validated_data.get('recipient')]).first()

      
        if not msg_session:
            mss = validated_data.pop("session", uuid.uuid4())
            
            msg_session = Session(id_session=mss, session_creator=user)
            msg_session.save()
            msg_session.members.set(
                [
                user,
                validated_data.get('recipient'),
            ],
            )

        message = Message(**validated_data)
        message.session = msg_session
        message.save()
        attachments = self.context.get("request").FILES.getlist("attachment")

        for attachment in attachments:
            sf = Attachment(attachment=message, file=attachment)
            sf.save()


        return message

    attachment = UserAttachmentListSerializer(required = False, many=True)
    session = UUIDField(required=False)

    class Meta:
        model = Message
        fields = ('session','recipient','text','reaction','date_created', 'attachment')

class MessageRectionSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"

    def update(self, instance, validated_data): 
        instance.reaction = validated_data.get('reaction', instance.reaction)
        instance.save()
        return instance
#############################################################

class UserSessionListSerializer(ModelSerializer):
    class Meta:
        model = Session
        fields = "__all__"


class UserMessageListSerializer(ModelSerializer):
    message = MessageSerializer(required=False, many=True)
    attachment = UserAttachmentListSerializer(required=False, many=True)

    class Meta:
        model = Message
        fields =  ('id','recipient','text','reaction','date_created', 'message', 'attachment',)
